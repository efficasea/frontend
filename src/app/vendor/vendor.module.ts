import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VendorRoutingModule } from './vendor-routing.module';

import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { AngularMaterialModule } from '../angular-material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AddVendorComponent } from './component/add-vendor/add-vendor.component';
import { BulkOfferComponent } from './component/bulk-offer/bulk-offer.component';
@NgModule({
  declarations: [AddVendorComponent, BulkOfferComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    VendorRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule,
    AngularMaterialModule,
    FlexLayoutModule
  ]
})
export class VendorModule { }
