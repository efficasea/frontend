import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BulkOfferComponent} from './component/bulk-offer/bulk-offer.component'

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'bulk-offer',
        component: BulkOfferComponent,
        data: {
            title: 'Order Request'
        }
    }
        
    ]
}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorRoutingModule { }
