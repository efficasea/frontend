import { Component, OnInit } from '@angular/core';
import { VendorService } from '../../vendor.service';

@Component({
  selector: 'app-bulk-offer',
  templateUrl: './bulk-offer.component.html',
  styleUrls: ['./bulk-offer.component.scss']
})
export class BulkOfferComponent implements OnInit {

  bulkOffers = [];
  localities = [];
  categories = [];
  selectedLocality = {name:'',id:''}
  selectedCategories : Array<any> = []
  selectedCategoriesString: String
  quantityTypes = [
    'kg',
    'gm',
    'tonn'
  ]

  selectedQuantityType : String

  constructor(private _vendorService:VendorService) { }

  ngOnInit(): void {

    this._vendorService.getBulkOffers().subscribe(  
      (res:Array<any>) => {
        this.bulkOffers = res
      },
      err => {
        console.log(err)
      }
    )

    this._vendorService.getAllCategories().subscribe(
      (res:Array<any>) => {
        console.log(res)
        this.categories = res
      },
      err => {
        console.log(err)
      }
    )

    this._vendorService.getAllLocalities().subscribe(
      (res:Array<any>) => {
        console.log(res)
        this.localities = res
      },
      err => {
        console.log(err)
      }
    )

  }

  onCreateBulkOffer(form){
    let data = form.value
    data.locality = this.selectedLocality.id;
    data.categories = this.selectedCategories.map(cat => {
      return cat.value._id
    })
    data['quantityType'] = this.selectedQuantityType;
    data['vendor'] = '5ea541a2bc48c96728ba8f65'
    this._vendorService.createBulkOffers(data).subscribe(
      res => {
        console.log(res)
      },
      err => {
        console.log(err)
      }
    )
  }

  onUpdateBulkOffer(form,bulkOfferId){
    let data = form.value;
    data['user'] = localStorage.getItem('uid')
    console.log(data,bulkOfferId)
    this._vendorService.updateBulkOffers(data,bulkOfferId).subscribe(
      res => {
        console.log(res)
      },
      err => {
        console.log(err)
      }
    )
  }

  onLocalitySelect(event){
    this.selectedLocality = {name:event.value.name,id:event.value._id}
    
  }

  onCategorySelect(event){
    this.selectedCategories.push(event)
    this.selectedCategoriesString = (this.selectedCategories.reduce( (list,obj) => {                                                              
      list.push(obj.value.name)
      return list
    },[])).join(',')
    
  }

}
