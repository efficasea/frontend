import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { StoreService } from './store.service';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss']
})
export class StoreComponent implements OnInit {
  store: any;
  categories: any;
  localities: any;

  constructor(
    private storeService: StoreService,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.store = null;
    this.categories = [];
    this.localities = [];
    this.activatedRoute.params.subscribe(params => {
      const storeId = params['id'];
      this.getStoreDetails(storeId);

    });
  }

  getStoreDetails(id){
    this.storeService.getStore(id).subscribe(
      (response: any) => {
        this.store = response;
        this.categories = response.categories;
        this.localities = response.localities;
      }
    )
  }
}
