import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
  })
  export class StoreService {
    constructor(private http: HttpClient) {}

    getStore(id){
        return this.http.get<any>(
            'http://localhost:1337/stores/'+id,
          );
    };

  }