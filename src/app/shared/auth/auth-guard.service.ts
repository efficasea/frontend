import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { UserAuthService } from './user.auth.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private authService: UserAuthService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    var token = localStorage.getItem('jwt');
    if (token && token != "" && typeof token != "undefined" && token != null) {
      return true;
    }
    else {
      this.router.navigate(['/auth/login']);
    }
    return false;
  }
}
