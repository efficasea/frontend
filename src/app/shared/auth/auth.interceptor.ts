import { HttpRequest, HttpInterceptor, HttpHandler, HttpEvent } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>,
              next: HttpHandler): Observable<HttpEvent<any>> {
        console.log(req.url.startsWith('/'),req.url)
        if (req.url.startsWith('/')) {
            const url = 'http://159.203.69.141:1337';
            
            req = req.clone({
                url: url + req.url
            });
        }
        return next.handle(req);
        // const idToken = localStorage.getItem("token");

        // if (idToken) {
        //     const cloned = req.clone({
        //         headers: req.headers.set("Authorization",
        //             "Bearer " + idToken)
        //     });

        //     return next.handle(cloned);
        // }
        // else {
        //     return next.handle(req);
        // }
    }
}