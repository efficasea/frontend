import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { HttpClient, HttpHeaders } from '@angular/common/http';
const httpOptions = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Origin': '*'
  })
};


@Injectable()
export class VendorAuthService {
  token: string;

  private loged = false;

  constructor(private http: HttpClient,
              private router: Router,
              public toastr: ToastrService
  ) { }

  signupUser(user) {
    const data = {
      name: user.name,
      email: user.email,
      password: user.password,
      role: localStorage.getItem('vrole'),
      mobile: user.mobile
      };
    const req = this.http.post('/auth/local/register', data, httpOptions)
      .subscribe(
        (res: any) => {
          console.log(res);
          const vdata = {name: user.vendorname, address: user.vendoraddress, gst: user.vendorgst, user: res.id};
          this.http.post('/vendors', vdata, httpOptions).subscribe(vres => {
            this.router.navigate(['/vendor-dashboard']);
            this.toastr.success('Vendor dashboard created!', 'Please visit your email for further details.', { closeButton: true });
          });
          this.toastr.success('Signup was successfull!', 'Please visit your email for further details.', { closeButton: true });
        },
        err => {
          console.log(err);
          this.toastr.error('There was some error in signup', '', { closeButton: true });
        });
  }

  signinUser(user) {
    const req = this.http.post('/auth/local', user, httpOptions)
      .subscribe(
        (res: any) => {
          localStorage.setItem('token', res.token);
          localStorage.setItem('uid', res.user._id);
          localStorage.setItem('cid', res.user.clientId);
          localStorage.setItem('rname', res.role.name);

          this.router.navigate(['user/dashboards']);
        },
        err => {
          console.log(err);
          this.toastr.error('Error: The username or password is incorrect', '', { closeButton: true });
        });

    // your code for checking credentials and getting tokens for for signing in user
    // this.loged = true;
  }
  forgotPassword(data) {
    const req = this.http.post('/api/forgot-password', data, httpOptions)
      .subscribe(
        res => {
          this.toastr.info('Request was successful. Please check you email for further process.', '', { closeButton: true });
        },
        err => {
          this.toastr.error('Error: Something went wrong please try again later.', '', { closeButton: true });
        });
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/auth/login']);
  }

  getToken() {
    return localStorage.getItem('token');
  }

  isAuthenticated() {
    const token = localStorage.getItem('token');
    if (token && token != '' && typeof token != 'undefined' && token != null) {
      return true;
    }
    return false;
  }
}
