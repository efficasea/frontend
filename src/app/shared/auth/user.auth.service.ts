import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { HttpClient, HttpHeaders } from '@angular/common/http';
const httpOptions = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Origin': '*'
  })
};


@Injectable()
export class UserAuthService {
  token: string;

  private loged = false;

  constructor(private http: HttpClient,
              private router: Router,
              public toastr: ToastrService
  ) { }

  signupUser(user) {
    const data = {
      name: user.name,
      email: user.email,
      password: user.password,
      mobile: user.mobile
      };
    const req = this.http.post('http://localhost:1337/auth/local/register', data)
      .subscribe(
        res => {
          console.log('success');
          this.router.navigate(['/inbox']);
          this.toastr.success('Signup was successfull!', 'Please visit your email for further details.', { closeButton: true });
        },
        err => {
          console.log(err);
          this.toastr.error('There was some error in signup', '', { closeButton: true });
        });
  }

  signinUser(user) {
    const req = this.http.post('http://localhost:1337/auth/local', user)
      .subscribe(
        (res: any) => {
          localStorage.setItem('jwt', res.jwt);
          localStorage.setItem('uid', res.user._id);
          localStorage.setItem('rid', res.role.id);
          localStorage.setItem('rname', res.role.name);
          this.router.navigate(['dashboard/dashboard1']);
        },
        err => {
          console.log(err);
          this.toastr.error('Error: The username or password is incorrect', '', { closeButton: true });
        });

    // your code for checking credentials and getting tokens for for signing in user
    // this.loged = true;
  }
  forgotPassword(data) {
    const req = this.http.post('/api/forgot-password', data, httpOptions)
      .subscribe(
        res => {
          this.toastr.info('Request was successful. Please check you email for further process.', '', { closeButton: true });
        },
        err => {
          this.toastr.error('Error: Something went wrong please try again later.', '', { closeButton: true });
        });
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/auth/login']);
  }

  getToken() {
    return localStorage.getItem('token');
  }

  isAuthenticated() {
    const token = localStorage.getItem('token');
    if (token && token != '' && typeof token != 'undefined' && token != null) {
      return true;
    }
    return false;
  }
}
