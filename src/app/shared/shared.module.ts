import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import { BannerComponent } from './component/banner/banner.component';
import { PageNotFoundComponent } from './component/page-not-found/page-not-found.component';
import { SearchComponent } from './component/search/search.component';
import { UserAuthService } from './auth/user.auth.service';
import { VendorAuthService } from './auth/vendor.auth.service';
import { ToastrService, ToastrModule } from 'ngx-toastr';


@NgModule({
  declarations: [BannerComponent, PageNotFoundComponent, SearchComponent],
  imports: [
    CommonModule,
    SharedRoutingModule,
    ToastrModule
  ],
  exports: [
    BannerComponent, PageNotFoundComponent, SearchComponent
  ],
  providers: [UserAuthService, VendorAuthService, ToastrService]
})
export class SharedModule { }
