import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserRoutingModule } from './user-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { RequestOrderComponent } from './component/request-order/request-order.component';
import { AccountComponent } from './component/account/account.component';
import { UserService } from './user.service';
import { AngularMaterialModule } from '../angular-material.module';


@NgModule({
  declarations: [RequestOrderComponent, AccountComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    UserRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule,
    AngularMaterialModule
  ],
  providers: [UserService]
})
export class UserModule { }
