import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RequestOrderComponent } from './component/request-order/request-order.component';


const routes: Routes = [
  {
    path: '',
    children: [
        {
            path: 'request-order',
            component: RequestOrderComponent,
            data: {
                title: 'Order Request'
            }
        }
    ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
