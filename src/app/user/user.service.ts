


import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class UserService {
  httpOptions : any
  constructor(private http: HttpClient) { 
    this.httpOptions = {
      headers: new HttpHeaders({
          'Content-Type': 'application/json',
          // 'Authorization': `Bearer ${localStorage.getItem('token')}`
      })
  };
  }

  getAllOrderRequests(){

    return this.http.get<any>(
      'http://159.203.69.141:1337/order-requests',
      // this.httpOptions
    )
  }

  getAllCategories(){
    return this.http.get<any>(
      'http://159.203.69.141:1337/categories',
      // this.httpOptions
    )
  }

  getAllLocalities(){
    return this.http.get<any>(
      'http://159.203.69.141:1337/localities',
      // this.httpOptions
    )
  }

  createOrderRequest(data){
    return this.http.post<any>(
      'http://159.203.69.141:1337/order-requests',
      JSON.stringify(data),
      this.httpOptions
    )
  }

  updateOrderRequest(data,orderRequestId){
    return this.http.put<any>(
      `http://159.203.69.141:1337/order-requests/${orderRequestId}`,
      JSON.stringify({'orderItems':[data]}),
      this.httpOptions
    )
  }

  getBulkOffers(){
    return this.http.get<any>(
      'http://159.203.69.141:1337/bulk-offers',
    )
  }

  createBulkOffers(data){
    return this.http.post<any>(
      'http://159.203.69.141:1337/bulk-offers',
      JSON.stringify(data),
      this.httpOptions

    )
  }

  updateBulkOffers(data,bulkOrderId){
    return this.http.put<any>(
      `http://159.203.69.141:1337/bulk-offers/${bulkOrderId}`,
      JSON.stringify(data),
      this.httpOptions

    )
  }

}

