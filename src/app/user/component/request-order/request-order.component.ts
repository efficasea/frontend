import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormArray, FormGroup, FormControl } from '@angular/forms';
import { UserService } from '../../user.service';

@Component({
  selector: 'app-request-order',
  templateUrl: './request-order.component.html',
  styleUrls: ['./request-order.component.scss']
})
export class RequestOrderComponent implements OnInit {

  orderRequests = [];
  localities = [];
  categories = [];
  selectedLocality = {name:'',id:''}
  selectedCategories : Array<any> = []
  selectedCategoriesString: String

  quantityTypes = [
    'kg',
    'gm',
    'tonn'
  ]

  selectedQuantityType : String

  panelOpenState = false;
  constructor(
    private formBuilder: FormBuilder,
    private _userService: UserService
  ) {

  }



  ngOnInit(): void {
    this._userService.getAllOrderRequests().subscribe(
      (res:Array<any>) => {
        console.log(res)
        this.orderRequests = res
      },
      err => {
        console.log(err)
      }
    )

    this._userService.getAllCategories().subscribe(
      (res:Array<any>) => {
        console.log(res)
        this.categories = res
      },
      err => {
        console.log(err)
      }
    )

    this._userService.getAllLocalities().subscribe(
      (res:Array<any>) => {
        console.log(res)
        this.localities = res
      },
      err => {
        console.log(err)
      }
    )




  }

  onCreateOrderRequest(form){
    let data = form.value
    console.log(data)
    console.log(this.selectedCategories)
    console.log(this.selectedLocality)
    data.locality = this.selectedLocality.id;
    data.categories = this.selectedCategories.map(cat => {
      return cat.value._id
    })

    this._userService.createOrderRequest(data).subscribe(
      res => {
        console.log(res)
      },
      err => {
        console.log(err)
      }
    )
  }

  onUpdateOrderRequest(form,orderRequestId){
    let data = form.value;
    data['quantityType'] = this.selectedQuantityType;
    data['user'] = localStorage.getItem('uid')
    this._userService.updateOrderRequest(data,orderRequestId).subscribe(
      res => {
        console.log(res)
      },
      err => {
        console.log(err)
      }
    )
  }

  onLocalitySelect(event){
    this.selectedLocality = {name:event.value.name,id:event.value._id}
    
  }

  onCategorySelect(event){
    this.selectedCategories.push(event)
    this.selectedCategoriesString = (this.selectedCategories.reduce( (list,obj) => {                                                              
      list.push(obj.value.name)
      return list
    },[])).join(',')
    
  }

  

}
