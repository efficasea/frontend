import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AddBusinessComponent } from './add-business/add-business.component';
import { StoreComponent } from './store/store.component';
import { SearchComponent } from './search/search.component';


const routes: Routes = [
  {
    path: "",
    component: HomeComponent
  },
  {
    path:"newbusiness",
    component: AddBusinessComponent
  },
  {
    path: "store/:id",
    component: StoreComponent
  },
  {
    path: "search",
    component: SearchComponent
  },
  {
    path: "user",
    loadChildren: () => import('./user/user.module').then(m => m.UserModule)
  },
  {
    path: "vendor",
    loadChildren: () => import('./vendor/vendor.module').then(m => m.VendorModule)
  },
  {
    path: "auth",
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
