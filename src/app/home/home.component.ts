import { Component, OnInit } from '@angular/core';
import { HomeService } from './home.service';
import $ from 'jquery/dist/jquery';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  categories: any;
  storeCount: number;
  stores: any;
  day: number;
  visitors: number;

  constructor(
    private homeService: HomeService,
  ) { }

  ngOnInit(): void {
    this.categories = [];
    this.storeCount = null;
    this.stores = [];
    this.visitors = null;
    this.getStoreCount();
    this.getStores();
    this.click_events();
  }


  getStores(){
    this.homeService.getAllStores().subscribe(
      (response: any) => {
        this.stores = response
      }
    )
  }

  getStoreCount(){
    this.homeService.getStoreCount().subscribe(
      (response: any) => {
        this.storeCount = response
      }
    )
  }
  click_events() {
    const self = this;
    $('#location').on('change paste keyup', function() {
      if ($(this).val().length >= 3) {
        self.autoCompleteLocation($(this).val());
      }
    });
  } 

  autoCompleteLocation(location) {
    this.homeService.autoCompleteLocation(location).subscribe(
      response => {
        if (response) {
          console.log(response);

          if (!Array.isArray(response)) {
            console.log('test');
          } else if (Array.isArray(response)) {
            let list = '';

            response.forEach(function(loca) {
              list += '<option><a routerLink=\'location/store/'+loca.id+'\'>'+loca.name+'</a></option>';
            });
            $('#location_suggestion').html(list);
          }
        } else {
          console.log('SORRY NO PORTS FOUND..:(');
        }
      },
      err => {
        console.log(err);
      }
    );
  }

}
