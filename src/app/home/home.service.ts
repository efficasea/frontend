import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
  })
  export class HomeService {
    constructor(private http: HttpClient) {}

    getAllStores(){
        return this.http.get<any>(
            'http://localhost:1337/stores/',
          );
    };

    getStoreCount(){
        return this.http.get<any>(
            'http://localhost:1337/stores/count',
          );
    }

    getAllCategories(){
        return this.http.get<any>(
            'http://localhost:1337/categories',
          );
    }

    autoCompleteLocation(location){
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      };
      return this.http.post<any>(
        "http://localhost:1337/places/search",
        JSON.stringify({"text":location}),
        httpOptions
      );
    }

  }