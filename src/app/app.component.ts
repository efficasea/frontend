import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Lockdown';
  categories: any;
  search: string;
  searchResults: any;

  constructor(
    private AppService: AppService,
  ) { }

  ngOnInit(): void {
    this.categories = [];
    this.getCategories();
    }

  getCategories(){
    this.AppService.getAllCategories().subscribe(
      (response: any) => {
        this.categories = response
      }
    )
  }  

  getSearchResults(search){
    this.AppService.search(search).subscribe(
      (response: any) => {
        this.searchResults = response;
      }
    )
  }
}
