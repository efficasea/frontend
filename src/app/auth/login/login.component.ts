import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.sevice'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private _authService:AuthService) { }

  ngOnInit(): void {
  }

  onLogin(form){
    console.log(form.value)
    this._authService.login(form.value.email,form.value.pass).subscribe(
      res => {
        console.log(res)
        localStorage.setItem('token',res.jwt);
        localStorage.setItem('uid',res.user._id);
      },
      err => {
        console.log(err)
      }
    )
  }

}
