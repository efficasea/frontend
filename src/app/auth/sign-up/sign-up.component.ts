import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.sevice'


@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  constructor(private _authService:AuthService) { }
  Roles: any = ['Vendor', 'User'];
    selected = ''
  ngOnInit(): void {
  }

  onSignup(form){
    this._authService.signup(form.value.email,form.value.pass,form.value.mobile,this.selected).subscribe(
      res => {
        console.log(res)
        localStorage.setItem('token',res.jwt);
        localStorage.setItem('uid',res.user._id);

      },
      err => {
        console.log(err)
      }
    )
  }

}
