import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    constructor(private http: HttpClient) { }

    login(email,pass) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };

        return this.http.post<any>(
            '/auth/local',
            JSON.stringify({identifier:email,password:pass}), 
            httpOptions
        )
    }


    signup(email,pass,mobile,role) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };

        return this.http.post<any>(
            '/auth/local/register',
            JSON.stringify({email:email,password:pass,mobile:mobile}), 
            httpOptions
        )
    }

}
