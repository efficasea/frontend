import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignUpComponent } from './sign-up/sign-up.component';
import { LoginComponent } from './login/login.component';


const routes: Routes = [
  {
    path: '',
    children: [
        {
            path: 'login',
            component: LoginComponent,
            data: {
                title: 'Login Page'
            }
        },
        {
            path: 'signup',
            component: SignUpComponent,
            data: {
                title: 'Sign Up Page'
            }
        },
        
    ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
