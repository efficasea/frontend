import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
  })
  export class AppService {
    constructor(private http: HttpClient) {}

    getAllCategories(){
        return this.http.get<any>(
            'http://localhost:1337/categories',
          );
    }

    search(ele){
      return this.http.get<any>(
          'http://localhost:1337/categories',
        );
  }

  }